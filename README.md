![vcut-logo](vcut.png)
## vcut
Make your own movies.  
Very simple handling by key commands and fast.

### Installation
Requires the installation of `mpv` and `ffmpeg` packages. The vcut script is stored as `/usr/local/bin/vcut` and is made executable with `chmod +x /usr/local/bin/vcut`.

###  Usage
On the first run, the script sets up the vcut folder into ~/Videos .
Videos (mp4) you want to cut, have to be placed into this work folder. From the second run you will see witch video is loaded and a list of key commands:  

<span style="color:green">Loaded: <span style="color:#00ffff">VID_20230628_083341.mp4</span>  
</span>
<span style="color:green">
Key commands: <span style="color:#ffff00">n</span>ext - <span style="color:#ffff00">l</span>ast - <span style="color:#ffff00">c</span>ut - <span style="color:#ffff00">u</span>nify - <span style="color:#ffff00">d</span>elete - <span style="color:#ffff00">q</span>uit  
</span>  
<span>&nbsp;</span>
#### Key Commands:
- `n [Enter]` - Load next video
- `l [Enter]` - Load last video
- `c [Enter]` - Initializise video cutting and starts mpv
  *  `s q` - Sets the startpoint of a cut and next `s q` - sets the endpoint of a cut
- `u [Enter]` - Put all cuts together and create your new video
- `d [Enter]` - Start a new projekt. Delete cuts in ~/Videos/vcut/cuts
- `q [Enter]` - Quit vcut

---
>Vcut will never delite cuts itself. This behavior garanties that you can restart vcut as often you want and missmatch your single cuts with a filemanager before you put all together with unify.  
---

## v2cut

v2cut is a reduced edition of the vcut script and is suitable for batch processing of videos that are cut only at the beginning and at the end.


<span style="color:green">Loaded: <span style="color:#00ffff">VID_20230628_083341.mp4</span>  
</span>
<span style="color:green">
Key commands: <span style="color:#ffff00">c</span>ut video <span style="color:#ffff00">q</span>uit  
</span>  
<span>&nbsp;</span>

### Configure Keys (mpv)
To set up (arrow) keys in mpv write these values to `~/.config/mpv/input.conf`.
With `frame-step` you can step frame by frame.
```

LEFT frame-back-step
RIGHT frame-step
UP seek 15
DOWN seek -5
```

### Configure window (mpv)
To configure the mpv window as you like, copy some of these values into `~/.config/mpv/mpv.conf`

- Start in fullscreen mode by default - `fs=yes`
- Start with centered window - `geometry=50%:50%`
- Start with 640x480 - `geometry=640x480`
- Start with position and size - `geometry=<[W[xH]][+-x+-y][/WS]>`
- Do not close the window on exit - `keep-open=yes`
- Start mpv without a video - `force-window=immediate`

### Sway
If you use sway as your Desktop uncomment `swaymsg` commands in vcut and configure them with applications and workspaces you have configured in ~/.config/sway/config. With this vcut changes focus automaticly.
For e.g: 
* assign [class="mpv"] workspace mpv
* assign [app_id="dolphin"] workspace dolphin
* assign [app_id=konsole] workspace konsole

### Contributions
Welcome
